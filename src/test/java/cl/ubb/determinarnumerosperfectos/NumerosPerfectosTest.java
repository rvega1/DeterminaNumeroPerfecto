package cl.ubb.determinarnumerosperfectos;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Test;

public class NumerosPerfectosTest {

	@Test
	public void IngresarSeisRetornaNumeroPerfectoVerdadero() {
		/*arrange*/
		NumerosPerfectos perfecto = new NumerosPerfectos();
		boolean resultado;
		
		
		
		/*act*/
		resultado = perfecto.determinarnumerosperfectos(6);
		
		
		
		/*assert*/
		assertThat(resultado,is(true));
		
		
	}

}
